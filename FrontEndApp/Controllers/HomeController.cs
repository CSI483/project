﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FrontEndApp.Models;
using Akka.Actor;
using CommunicationLibrary;

namespace FrontEndApp.Controllers
{
    public class HomeController : Controller
    {
        public static ActorSystem myActorSystem;

        public IActionResult Index()
        {
            //Create Initializer which has the instances
            //of the ActorSystem that we need
            Initializer initializer = new Initializer();

            //Get and reference the ActorSystem from Initializer
            myActorSystem = initializer.MyActorSystem;



            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
