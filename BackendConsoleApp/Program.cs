﻿using System;
using Akka.Actor;
using CommunicationLibrary;

namespace BackendConsoleApp
{
    class Program
    {
        public static ActorSystem myActorSystem;

        static void Main(string[] args)
        {
            //Create Initializer which has the instances
            //of the ActorSystem that we need
            Initializer initializer = new Initializer();

            //Get and reference the ActorSystem from Initializer
            myActorSystem = initializer.MyActorSystem;
            
            Console.WriteLine("Hello World! This is a test of the git system!");
            Console.ReadLine();
            
            myActorSystem.ActorSelection("akka://myActorSystem/user/frontendActor").
                Tell(new Message("This is a message from the backend", true));

            Console.ReadLine();
        }
    }
}
