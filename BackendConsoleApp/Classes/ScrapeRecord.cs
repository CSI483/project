﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendConsoleApp.Classes
{
    //The ScrapeRecord class is used to input information into the database to track URL's scraped, terms used, and what email address they are associated with. 
    public class ScrapeRecord
    {
        private string targetURL { get; } //generic URL user is intending to crawl through. E.g. a specific subreddit or craigslist location
        private string term { get; } //word or phrase user is searching for.
        private string scrapeResultURL { get; } //instances where the search term matches an existing entry. This will be checked against existing scrapeResultURL's for duplicates
        private string email { get; } //Email at which user will receive notification

        //Constructor for creating a new instance of the ScrapeRecord class
        public ScrapeRecord(string targetURL, string term, string scrapeResultURL, string email)
        {
            this.targetURL = targetURL;
            this.term = term;
            this.scrapeResultURL = scrapeResultURL;
            this.email = email;
        }



    }
}
