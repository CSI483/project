﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace BackendConsoleApp.Classes
{
    /// <summary>
    /// Allows for passing of commands to the command linux command line
    /// </summary>
    public static class EchoToCLI
    {
        public static string Bash(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            return result;
        }
    }
}
