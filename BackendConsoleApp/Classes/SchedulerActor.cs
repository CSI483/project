﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Akka;
using Akka.Actor;
using NLog.Common;
using Microsoft.Extensions.Logging;


namespace BackendConsoleApp.Classes
{
    public class SchedulerActor : UntypedActor
    {

        protected override void PreStart()
        {
            //pre-start area
        }

        protected override void OnReceive(object message)
        {
            //Reflector gets all message properties as a dictionary in property ReturnValue
            PropertyReflector reflector = new PropertyReflector(message);
            StringBuilder sb = new StringBuilder();
            //Match pattern for digits
            Regex pattern = new Regex(@"\d+$");
            if (reflector.ReturnValue.ContainsKey("Placeholder"))
            {
                foreach(KeyValuePair<string, object>nameValue in reflector.ReturnValue)
                {
                    if(pattern.IsMatch(nameValue.Value.ToString()))
                    {
                        sb.Append($"{nameValue.Value.ToString()} ");
                    }
                                 
                }
                //TODO create schedule here
                var cronDirectory = $"cd \"etc/chron.d/\"".Bash();
                var cronAddSchedule = $"echo \"{sb.ToString()} PlaceHolder >> Jobs\"".Bash();

            }
            else
            {
                //Error Logging, etc
            }
        }


    }
}

