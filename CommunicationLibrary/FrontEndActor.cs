﻿using System;
using System.Collections.Generic;
using System.Text;
using Akka.Actor;

namespace CommunicationLibrary
{
    public class FrontendActor : UntypedActor
    {
        private string _messageReceived = "";

        #region Constructors
        public FrontendActor() { }

        #endregion

        public string MessageReceived { get { return _messageReceived; } }

        #region Methods
        protected override void OnReceive(object message)
        {
            if (message is Message)
            {
                var msg = message as Message;
                _messageReceived = msg.Reason;

                if (msg.SendOnce)
                {
                    Context.ActorSelection("akka://myActorSystem/user/backendActor").
                        Tell(new Message("Message received by front end", false));
                }
            }
        }

        #region Internal Methods
        #endregion
        #endregion
    }
}