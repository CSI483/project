﻿using System;
using System.Collections.Generic;
using System.Text;
using Akka.Actor;

namespace CommunicationLibrary
{
    public class Initializer
    {
        private static ActorSystem _myActorSystem;
        private IActorRef _backendActor;
        private IActorRef _frontendActor;

        public Initializer()
        {
            //-If these don't exist, create them!
            //-This is here so there are no duplicates of 
            // these objects, and so the program can still
            // access them

            //-Check if _myActorSystem doesn't exist, if it
            // doesn't, make it
            //-This is here to prevent more than one Actor
            // System being created
            if(_myActorSystem == null)
            {
                _myActorSystem = ActorSystem.Create("myActorSystem");
            }
            if(_backendActor == null)
            {
                Props backendActorProps = Props.Create<BackendActor>();
                _backendActor = _myActorSystem.ActorOf(backendActorProps, "backendActor");
            }
            if (_frontendActor == null)
            {
                Props frontendActorProps = Props.Create<FrontendActor>();
                _frontendActor = _myActorSystem.ActorOf(frontendActorProps, "frontendActor");
            }
        }

        public ActorSystem MyActorSystem { get { return _myActorSystem; } }

        #region Methods

        #endregion
    }
}
