﻿using System;
using System.Collections.Generic;
using System.Text;


namespace CommunicationLibrary
{
    public class Message
    {
        public Message(string reason, bool sendOnce)
        {
            Reason = reason;
            SendOnce = sendOnce;
        }

        public string Reason { get; private set; }
        public bool SendOnce { get; private set; }
    }
}
