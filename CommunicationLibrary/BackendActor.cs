﻿using System;
using System.Collections.Generic;
using System.Text;
using Akka.Actor;

namespace CommunicationLibrary
{
    public class BackendActor : UntypedActor
    {
        private string _messageReceived = "";

        #region Constructors
        public BackendActor() { }

        #endregion

        public string MessageReceived { get { return _messageReceived; } }

        #region Methods
        protected override void OnReceive(object message)
        {
            if (message is Message)
            {
                var msg = message as Message;
                _messageReceived = msg.Reason;

                if (msg.SendOnce)
                {
                    Context.ActorSelection("akka://myActorSystem/user/frontendActor").
                        Tell(new Message("Message received by the back end", false));
                }

                Console.WriteLine("Message from Frontend: " + _messageReceived);
            }
            else
            {
                Console.WriteLine("Message not recognized");
            }
        }

        #region Internal Methods
        #endregion
        #endregion
    }
}
